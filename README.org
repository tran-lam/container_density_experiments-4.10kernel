* Kernel dev notes
** config options
*** http://www.linux.org/threads/the-linux-kernel-configuring-the-kernel-part-1.4274/
** needed to patch aufs into kernel
*** see http://forum.lemaker.org/thread-442-1-1.html
    - for XXX.pach, it is the name of each patch
* tcp_sendmsg
** sk_stream_alloc
   - seems to allocate skb. Location where data place into socket buffer?
** tcp_push_one
*** tcp_write_xmit
**** tcp_transmit_skb
     - err = icsk->icsk_af_ops->queue_xmit(sk, skb, &inet->cork.fl); 
       This does ip_queue_xmit.
***** ...
***** __dev_xmit_skb
      - enqueues onto qdisc, can by bypassed to sched_direct_xmit
* qdisc outgoing
** qdisc_restart
   - dequeues packets
*** sched_direct_xmit
**** dev_hard_start_xmit
***** xmit_one
****** trace net_dev_start_xmit
* softirq
** net_rx_action
   - in dev.c
   - trace_napi_poll
* kfree_skb
** skb_release_head_state
*** tcp_wfree
**** destructor
     - assigned in tcp_transmit_skb
     - will signal tcp small queues to be able run again, tcp_xmit_one path
     - generally called on a tx software irq
* tcp_tasklet_func
  - handles tcp small queues handling allowing more things to write
  - raised on tx interrupt
